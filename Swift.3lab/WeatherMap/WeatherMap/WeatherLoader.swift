//
//  WeatherLoader.swift
//  WeatherMap
//
//  Created by Admin on 03.03.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import Foundation
import SwiftyJSON

class WeatherLoader{
    
    private var cities : [CityWeatherDescription]
    private var tableToUpdate : UITableView
    
    init(forCities cities : [CityWeatherDescription], tableToUpdate : UITableView){
        self.cities = cities
        self.tableToUpdate = tableToUpdate
    }
    
    func loadWeather(){
        
        var index = 0
        
        while index < cities.count{
            let citiesToLoad = (cities.count - index > 3 ? 3 : cities.count - index)
            let request = createRequest(firstIndex: index, lastIndex: index + citiesToLoad - 1)
            loadCitiesData(request: request, index: index)
            index += citiesToLoad
        }
    }
    
    private func createRequest(firstIndex: Int, lastIndex: Int) -> String{
        var request = "http://api.openweathermap.org/data/2.5/group?id="
        
        for i in firstIndex...(lastIndex - 1) {
            request += String(cities[i].id) + ","
        }
        request += String(cities[lastIndex].id)
        request += "&APPID=a96a20b99d33a4328355077b4b928f61"
        
        return request
    }
    
    func loadWeather(forCoordinate lon : Double, lat : Double) -> CityWeatherDescription!{
        return nil
    }
    
    private func loadCitiesData(request : String, index: Int){
        var index = index
        let url = URL(string: request)
        URLSession.shared.dataTask(with: url!){
            data, response, error in
            guard error == nil else{
                print(error!)
                return
            }
            
            guard let content = data else{
                return
            }
            
            let jsonData = JSON(data: content, options: JSONSerialization.ReadingOptions.mutableContainers, error: nil)
            //NSLog(jsonData.description)
            let cityList = jsonData["list"].array!
            
            for city in cityList{
                let weather = city["weather"].array
                self.cities[index].description = (weather?[0]["description"].description)!
                self.cities[index].pressure = city["main"]["pressure"].doubleValue.roundTo(places: 2)
                let temperatureInCelcius = city["main"]["temp"].doubleValue - 273.15
                self.cities[index].temperatureInCelcius = temperatureInCelcius.roundTo(places: 2)
                self.cities[index].windSpeed = city["wind"]["speed"].doubleValue.roundTo(places: 2)
                
                index += 1
            }
            
            self.tableToUpdate.reloadData()
            
            }.resume()

    }
}

extension Double{
    func roundTo(places : Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (divisor * self).rounded() / divisor;
    }
}
