//
//  WeatherMapViewController.swift
//  WeatherMap
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import UIKit
import MapKit

class WeatherMapViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var weatherMap: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let location = CLLocationCoordinate2DMake(51.506794, -0.125637)
        let region = MKCoordinateRegionMake(location, span)
        weatherMap.setRegion(region, animated: true)
        
        let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleMapTap(_recognizer:)))
        recognizer.minimumPressDuration = 0.5
        recognizer.delegate = self
        weatherMap.addGestureRecognizer(recognizer)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    func handleMapTap(_recognizer: UILongPressGestureRecognizer) {
        
        if(_recognizer.state == UIGestureRecognizerState.began)
        {
            let touchPoint = _recognizer.location(in: self.weatherMap)
            let tappedCoordinate = self.weatherMap.convert(touchPoint, toCoordinateFrom: self.weatherMap)
            let cityWeatherDescription = findNearestCity(lon: tappedCoordinate.longitude, lat: tappedCoordinate.latitude)
            
            let span = MKCoordinateSpanMake(0.1, 0.1)
            let location = CLLocationCoordinate2DMake(cityWeatherDescription.lat, cityWeatherDescription.lon)
            let region = MKCoordinateRegionMake(location, span)
            weatherMap.setRegion(region, animated: true)
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = location
            annotation.title = cityWeatherDescription.city + ": " + cityWeatherDescription.description
            annotation.subtitle = "Temperature: " + String(cityWeatherDescription.temperatureInCelcius) + "C, " + "Pressure: " + String(cityWeatherDescription.pressure)
            self.weatherMap.removeAnnotations(weatherMap.annotations)
            self.weatherMap.addAnnotation(annotation)
        }
    }
    
    private func findNearestCity(lon: Double, lat: Double) -> CityWeatherDescription{
        let cities = GlobalVariables.cities!
        let tappedPoint = CLLocation(latitude: lat, longitude: lon)
        var minDistance = 99999999999.0
        var nearestCity = cities[0]
        for city in cities{
            let cityPoint = CLLocation(latitude: city.lat, longitude: city.lon)
            let distance = cityPoint.distance(from: tappedPoint)
            if minDistance > distance.binade{
                minDistance = distance.binade
                nearestCity = city
            }
        }
        
        return nearestCity
    }
}
