//
//  WeatherTableViewController.swift
//  WeatherMap
//
//  Created by Admin on 03.03.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import UIKit

class WeatherTableViewController: UITableViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let dataReader = CitiesJsonFileReader()
        GlobalVariables.cities = dataReader.parceJsonFile()
    
        if GlobalVariables.cities == nil{
            showAlert()
            return
        }
        let loader = WeatherLoader(forCities: GlobalVariables.cities!, tableToUpdate: self.tableView)
        loader.loadWeather()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if GlobalVariables.cities != nil{
            return GlobalVariables.cities!.count
        }
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as! WeatherTableViewCell
        let row = indexPath.row
        
        let cities = GlobalVariables.cities!
        cell.cityLabel.text = cities[row].countryCode + ", " + cities[row].city
        cell.pressureLabel.text = "Pressure: " + String(cities[row].pressure)
        cell.descriptionLabel.text = cities[row].description
        cell.temperatureLabel.text = "Temperature: " + String(cities[row].temperatureInCelcius) + "C"
        cell.windSpeedLabel.text = "Wind speed: " + String(cities[row].windSpeed)

        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "weatherDetailed", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let cities = GlobalVariables.cities else{
            return
        }
        
        let row = (sender as! IndexPath).row
        
        if let detailedVC = segue.destination as? WeatherDetailedViewController {
            detailedVC.cityWeatherDescription = cities[row]
        }
        
    }
    
    @IBAction func showAlert(){
        let alertController = UIAlertController(title: "Internal error", message: "Can't display data", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        
        present(alertController, animated: true, completion: nil)
    }


}
